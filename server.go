package main

import (
	"os"

	"github.com/lacnguyen/gqlgen-todos/http"
	"github.com/lacnguyen/gqlgen-todos/middleware"

	// "github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
)

const defaultPort = ":5000"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	server := gin.Default()

	server.Use()

	server.GET("/", http.PlaygroundHandler())
	server.POST("/query", middleware.AuthorizeJWT(), http.GraphQLHandler())

	server.Run(defaultPort)

}
